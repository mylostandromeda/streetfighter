import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let leftFighterHealth = firstFighter.health;
    let rightFighterHealth = secondFighter.health;
    let firstFighterPunch = new Audio("../resources/sounds/punch1.mp3");
    let secondFighterPunch = new Audio("../resources/sounds/punch2.mp3");
    let criticalHit = new Audio("../resources/sounds/critical-hit.mp3");
    
    initControls(controls, firstFighter, secondFighter, resolve, leftFighterHealth, rightFighterHealth, firstFighterPunch, secondFighterPunch, criticalHit);
  });
}

export function initControls(controls, firstFighter, secondFighter, resolve, leftFighterHealth, rightFighterHealth, firstFighterPunch, secondFighterPunch, criticalHit) {
  const keys =  Object.values(controls)
                .flatMap((e) => { return e})
                .reduce((object,item) => { object[item] = false; return object}, {})
  
  let leftBar = document.getElementById('left-fighter-indicator');
  let rightBar = document.getElementById('right-fighter-indicator');
  let firstFighterDelay = false;
  let secondFighterDelay = false;

  document.addEventListener("keydown", function (event) {
    if(keys.hasOwnProperty(event.code)) {
      keys[event.code] = true;
    }
    if(event.code == controls.PlayerOneAttack) {
      if (keys.KeyL && !event.repeat) {
        firstFighterPunch.play();
      } else if(!keys.KeyD && !event.repeat && !keys.keyL) {
        firstFighterPunch.play();
        rightFighterHealth -= getDamage(firstFighter, secondFighter);
      }   
    } 
    if(event.code == controls.PlayerTwoAttack) {
      if (keys.KeyD && !event.repeat) {
        secondFighterPunch.play();
      } else if(!keys.KeyL && !event.repeat && !keys.keyD) {
        secondFighterPunch.play();
        leftFighterHealth -= getDamage(secondFighter, firstFighter);
      }
    }   
    if(keys.KeyQ && keys.KeyW && keys.KeyE) {
      if(firstFighterDelay === false) {
        criticalHit.play();
        rightFighterHealth -= criticalDamage(firstFighter);
        firstFighterDelay = true;
        setTimeout(() => firstFighterDelay = false, 10000);
      } 
    }
    if(keys.KeyU && keys.KeyI && keys.KeyO) {
      if(secondFighterDelay === false) {
        criticalHit.play();
        leftFighterHealth -= criticalDamage(secondFighter);
        secondFighterDelay = true;
        setTimeout(() => secondFighterDelay = false, 10000);
      } 
    }

    changeBar(rightBar, rightFighterHealth, secondFighter.health);
    changeBar(leftBar, leftFighterHealth, firstFighter.health);

    getWinner(leftFighterHealth, rightFighterHealth, firstFighter, secondFighter, resolve);
  });

  keyUpEventListener(keys);
}

export function getDamage(attacker, defender) {
  let attack = getHitPower(attacker) - getBlockPower(defender);
  attack = Math.max(0, attack);
  return attack;
}

export function getHitPower(attack) {
  let power = attack.attack * criticalHitChance(1,2);

  return power;
}

export function getBlockPower(defense) {
  let power = defense.defense * dodgeChance(1,2);

  return power;
}

export function criticalDamage(attack) {
  const damage = attack.attack * 2;

  return damage;
}

export function criticalHitChance(min, max) {
  return Math.random() * (max - min) + min;
}

export function dodgeChance(min, max) {
  return Math.random() * (max - min) + min;
}

export function keyUpEventListener(keys) {
  document.addEventListener("keyup", function (event) {
    if(keys.hasOwnProperty(event.code)) {
      keys[event.code] = false;
    }
  })
}

export function changeBar(bar, actualHealth, defaultHealth) {
  if (actualHealth <= 0) {
    bar.style.width = `0%`;
  }
  bar.style.width = `${(actualHealth * 100) / defaultHealth}%`;
}

export function getWinner(leftBar, rightBar, firstFighter, secondFighter, resolve){
  if(leftBar <= 0) {
    resolve(secondFighter);
  } else if(rightBar <= 0) {
    resolve(firstFighter);
  }
}