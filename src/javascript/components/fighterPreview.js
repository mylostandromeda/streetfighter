import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if(!fighter) return fighterElement;
  
  fighterElement.append(
    createFighterName(fighter),
    createFighterImage(fighter),
    createFighterInfoElement('Health', fighter.health, 'fighter-preview___health'),
    createFighterInfoElement('Attack', fighter.attack, 'fighter-preview___attack'),
    createFighterInfoElement('Defence', fighter.defense, 'fighter-preview___defense')
  );

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

export function createFighterName(fighter){
  const { name } = fighter;

  const nameElement = createElement({
    tagName: 'div',
    className: `fighter-preview___name`,
  });

  nameElement.append(fighter.name);

  return nameElement
}

export function createFighterInfoElement(name, value, className){
  const fighterElement = createElement({
    tagName: 'div',
    className: className,
  });
  const nameElement = createElement({
    tagName: 'span',
    className: 'fighter-preview___info-name'
  });
  const infoElement = createElement({
    tagName: 'span',
    className: 'fighter-preview___info-value'
  });
  nameElement.append(name + ': ');
  infoElement.append(value);
  fighterElement.append(nameElement, infoElement);

  return fighterElement;
}