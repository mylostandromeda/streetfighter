import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  showModal({ title: "The winner is " + fighter.name, bodyElement: createFighterImage(fighter)});
}
